import requests
from requests.auth import HTTPBasicAuth
import urllib3
import argparse
import getpass
import json
from base64 import b64encode
import sys
import os
import getopt


def post_call(url, payload, headers):
  try:
      response = requests.request("POST", url, data=payload, headers=headers,
                                  verify=False)
      if(response.ok):
          print(response.text)
      else:
          print(f'An error occurred while connecting to {clusterip}.')
          # the following line can be uncommented to show detailed error information
          print(response.text)
  except Exception as ex:
      print(f'An {type(ex).__name__} exception occurred while connecting to {clusterip}.\nArgument: {ex.args}.')

  #j = json.loads(response.text)
  return json.loads(response.text)


def get_call(url, headers):
  try:
      response = requests.request("GET", url, headers=headers,
                                  verify=False)
      if(response.ok):
          print(response.text)
      else:
          print(f'An error occurred while connecting to {clusterip}.')
          # the following line can be uncommented to show detailed error information
          print(response.text)
  except Exception as ex:
      print(f'An {type(ex).__name__} exception occurred while connecting to {clusterip}.\nArgument: {ex.args}.')

  #j = json.loads(response.text)
  return json.loads(response.text)


def delete_call(url, headers):
  try:
      response = requests.request("DELETE", url, headers=headers,
                                  verify=False)
      if(response.ok):
          print(response.text)
      else:
          print(f'An error occurred while connecting to {clusterip}.')
          # the following line can be uncommented to show detailed error information
          print(response.text)
  except Exception as ex:
      print(f'An {type(ex).__name__} exception occurred while connecting to {clusterip}.\nArgument: {ex.args}.')

  #j = json.loads(response.text)
  return json.loads(response.text)


def poll_task(url, headers):
  print("Polling task for completion")
  while True:
    r = get_call(url, headers)

    if re.match(".*SUCCEEDED.*", r['status']):
      break
    if re.match(".*RUNNING.*", r['status']):
      print(".... Task currently in status Running, waiting to complete....")
      time.sleep(1)
    else:
      print(f"Task failed with {r['status']}")
  return


'''
suppress warnings about insecure connections
you probably shouldn't do this in production
'''
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# get the arguments passed to the script
clusterip = 'pc_ip'
username = 'username'
password = 'password'
vmname = 'vmname'

# setup the URL that will be used for the API request
base_url = f"https://{clusterip}:9440/PrismGateway/services/rest/v2.0"

encoded_credentials = b64encode(bytes(f"{username}:{password}",
                                encoding="ascii")).decode("ascii")
auth_header = f'Basic {encoded_credentials}'

headers = {
    'Accept': "application/json",
    'Content-Type': "application/json",
    'Authorization': f"{auth_header}",
    'cache-control': "no-cache"
    }

vm_url = f"{base_url}/vms/?filter=vm_name%3D%3D{vmname}"
vm_info = get_call(vm_url, headers)
vm_uuid = vm_info['entities'][0]['uuid']

nic_url = f"{base_url}/vms/{vm_uuid}/nics"
nic_info = get_call(nic_url, headers)

for nics in nic_info['entities']:
  nic_uuid = nics['mac_address']
  nic_delete_url = f"{base_url}/vms/{vm_uuid}/nics/{nic_uuid}"
  print(nic_delete_url)

  call_return = delete_call(nic_delete_url, headers)

  poll_url = f"{base_url}/tasks/poll"
  poll_task(poll_url, call_return['task_uuid'], headers)

