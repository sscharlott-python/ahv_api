import json
from influxdb import InfluxDBClient
from flask import Flask, request, Response
from datetime import datetime

import mysql.connector
from mysql.connector import Error

app = Flask(__name__)

@app.route('/webhook', methods=['POST'])
def respond():
  post_data=request.json

  print(post_data['event_type'])
  print(post_data['data']['metadata']['status']['name'])
  print(post_data['entity_reference']['uuid'])
  
  now = datetime.now()
  current_time = now.strftime("%H:%M")
  print(current_time)


  ##
  ## UPDATE MYSQL DB
  ##

  try:
    connection = mysql.connector.connect(host='localhost',
                                         database='vm_active_data',
                                         user='root',
                                         password='password')
    if connection.is_connected():
        db_Info = connection.get_server_info()
        print("Connected to MySQL Server version ", db_Info)
        cursor = connection.cursor()

        sql1 = """
        create table if not exists vm_actions (
          name VARCHAR(255),
          action VARCHAR(255),
          time VARCHAR(255),
          uuid VARCHAR(255)
        )
        """

        cursor.execute(sql1)

        sql2 = """
        insert into vm_actions (name, action, time, uuid)
        VALUES (%s, %s, %s, %s)
        """

        val = (post_data['data']['metadata']['status']['name'], post_data['event_type'], current_time, post_data['entity_reference']['uuid'])

        #cursor.execute("create table if not exists vm_actions (name VARCHAR(255), action VARCHAR(255), time VARCHAR(255), uuid VARCHAR(255));")
        cursor.execute(sql2, val)

        connection.commit()

  except Error as e:
    print("Error while connecting to MySQL", e)
  finally:
    if (connection.is_connected()):
        cursor.close()
        connection.close()
        print("MySQL connection is closed")


  ##
  ## UPDATE INFLUXDB
  ##

  query = 'select * from vm_uptime'
  client = InfluxDBClient("localhost", "8086", "root", "password")
  dbs = client.get_list_database()
  if 'vm_uptime_info' not in dbs:
    client.create_database('vm_uptime_info')

  client.switch_database('vm_uptime_info')  

  if post_data['event_type'] == 'VM.ON':
    state = 1
  else:
    state = 0

  json_body = [
    {
      "measurement": "vm_uptime",
      "tags": {
        "vm": post_data['data']['metadata']['status']['name'],
        "type": post_data['event_type']
      },
      "fields": {
        "vm_name": post_data['data']['metadata']['status']['name'],
        "vm_uuid": post_data['entity_reference']['uuid'],
        "event_type": post_data['event_type'],
        "state": state
      }
    }
  ]

  client.write_points(json_body)
  result = client.query(query)
  print("Result: {0}".format(result))


  return Response(status=200)
