import requests
from requests.auth import HTTPBasicAuth
import urllib3
import argparse
import getpass
import json
from base64 import b64encode
import sys
import os
import getopt


def post_call(url, payload, headers):
  try:
      response = requests.request("POST", url, data=payload, headers=headers,
                                  verify=False)
      if(response.ok):
          print(response.text)
      else:
          print(f'An error occurred while connecting to {clusterip}.')
          # the following line can be uncommented to show detailed error information
          print(response.text)
  except Exception as ex:
      print(f'An {type(ex).__name__} exception occurred while connecting to {clusterip}.\nArgument: {ex.args}.')

  #j = json.loads(response.text)
  return json.loads(response.text)


def put_call(url, payload, headers):
  try:
      response = requests.request("PUT", url, data=payload, headers=headers,
                                  verify=False)
      if(response.ok):
          print(response.text)
      else:
          print(f'An error occurred while connecting to {clusterip}.')
          # the following line can be uncommented to show detailed error information
          print(response.text)
  except Exception as ex:
      print(f'An {type(ex).__name__} exception occurred while connecting to {clusterip}.\nArgument: {ex.args}.')

  #j = json.loads(response.text)
  return json.loads(response.text)


def get_call(url, headers):
  try:
      response = requests.request("GET", url, headers=headers,
                                  verify=False)
      if(response.ok):
          print(response.text)
      else:
          print(f'An error occurred while connecting to {clusterip}.')
          # the following line can be uncommented to show detailed error information
          print(response.text)
  except Exception as ex:
      print(f'An {type(ex).__name__} exception occurred while connecting to {clusterip}.\nArgument: {ex.args}.')

  #j = json.loads(response.text)
  return json.loads(response.text)


def delete_call(url, headers):
  try:
      response = requests.request("DELETE", url, headers=headers,
                                  verify=False)
      if(response.ok):
          print(response.text)
      else:
          print(f'An error occurred while connecting to {clusterip}.')
          # the following line can be uncommented to show detailed error information
          print(response.text)
  except Exception as ex:
      print(f'An {type(ex).__name__} exception occurred while connecting to {clusterip}.\nArgument: {ex.args}.')

  #j = json.loads(response.text)
  return json.loads(response.text)


def poll_task(url, task_uuid, headers):
  specs = []
  specs.append(task_uuid)
  uuidSpec = {}
  uuidSpec["completed_tasks"] = specs

  while True:
    print("Polling task %s for completion" % (task_uuid,))
    payload=json.dumps(uuidSpec)
    r = post_call(url, payload, headers)

    for entry in r['completed_tasks_info']:
      mr = entry['meta_response']['error_code']

    if mr is None:
      continue
    if mr is 0:
      break
    else:
      raise Exception("Task %s failed with error code: %s" %
          (task_uuid, mr))


'''
suppress warnings about insecure connections
you probably shouldn't do this in production
'''
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# get the arguments passed to the script
clusterip = 'prism central IP'
username = 'username'
password = 'password'
vmname = 'vmname'

# setup the URL that will be used for the API request
base_url = f"https://{clusterip}:9440/api/nutanix/v3"

encoded_credentials = b64encode(bytes(f"{username}:{password}",
                                encoding="ascii")).decode("ascii")
auth_header = f'Basic {encoded_credentials}'

headers = {
    'Accept': "application/json",
    'Content-Type': "application/json",
    'Authorization': f"{auth_header}",
    'cache-control': "no-cache"
    }

payload = {
  "kind": "vm",
  "filter": f"vm_name=={vmname}"
}

vm_url = f"{base_url}/vms/list"
vm_info = post_call(vm_url, json.dumps(payload), headers)

print(json.dumps(vm_info, indent=2))

vm_spec = vm_info['entities'][0]
vm_uuid = vm_spec['metadata']['uuid']
del vm_spec['status']

json_new_mac = {
   "mac_address" : "50:6b:8d:c3:45:b1"
   }

json_new_nics = {
   "nic_list": [
     {
       "nic_type": "NORMAL_NIC",
       "vlan_mode": "ACCESS",
       "mac_address": "50:6b:8d:c3:45:b1",
       "subnet_reference": {
         "kind": "subnet",
          "name": "AHV02_IPAM",
          "uuid": "7923f6f6-483f-4dea-ade9-a02d781139f9"
          },
      "is_connected": True,
      }
  ]
}

update_url = f"{base_url}/vms/{vm_uuid}"

vm_spec['spec']['resources'].update(json_new_nics)
return_info = put_call(update_url, json.dumps(vm_spec), headers)

print(json.dumps(return_info, indent=2))
